<?php

namespace App\Repository;

use App\Entity\Tentacule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Tentacule|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tentacule|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tentacule[]    findAll()
 * @method Tentacule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TentaculeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tentacule::class);
    }

    // /**
    //  * @return Tentacule[] Returns an array of Tentacule objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Tentacule
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
