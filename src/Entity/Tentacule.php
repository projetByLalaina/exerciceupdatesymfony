<?php

namespace App\Entity;

use App\Repository\TentaculeRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass=TentaculeRepository::class)
 * @ApiResource
 */
class Tentacule
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\ManyToOne(targetEntity=Kraken::class, inversedBy="Tentacules")
     */
    private $Kraken;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getKraken(): ?Kraken
    {
        return $this->Kraken;
    }

    public function setKraken(?Kraken $Kraken): self
    {
        $this->Kraken = $Kraken;

        return $this;
    }
}
