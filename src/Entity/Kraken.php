<?php

namespace App\Entity;

use App\Repository\KrakenRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=KrakenRepository::class)
 * @ApiResource
 */
class Kraken
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0)
     */
    private $age;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0)
     */
    private $taille;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0)
     */
    private $poids;

    /**
     * @ORM\OneToMany(targetEntity=Tentacule::class, mappedBy="Kraken")
     */
    private $Tentacules;

    /**
     * @ORM\OneToMany(targetEntity=Pouvoir::class, mappedBy="kraken")
     */
    private $Pouvoirs;

    public function __construct()
    {
        $this->Tentacules = new ArrayCollection();
        $this->Pouvoirs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getAge(): ?string
    {
        return $this->age;
    }

    public function setAge(string $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getTaille(): ?string
    {
        return $this->taille;
    }

    public function setTaille(string $taille): self
    {
        $this->taille = $taille;

        return $this;
    }

    public function getPoids(): ?string
    {
        return $this->poids;
    }

    public function setPoids(string $poids): self
    {
        $this->poids = $poids;

        return $this;
    }

    public function getPouvoir(): ?Pouvoir
    {
        return $this->pouvoir;
    }

    public function setPouvoir(?Pouvoir $pouvoir): self
    {
        $this->pouvoir = $pouvoir;

        return $this;
    }

    /**
     * @return Collection|Tentacule[]
     */
    public function getTentacules(): Collection
    {
        return $this->Tentacules;
    }

    public function addTentacule(Tentacule $tentacule): self
    {
        if (!$this->Tentacules->contains($tentacule)) {
            $this->Tentacules[] = $tentacule;
            $tentacule->setKraken($this);
        }

        return $this;
    }

    public function removeTentacule(Tentacule $tentacule): self
    {
        if ($this->Tentacules->removeElement($tentacule)) {
            // set the owning side to null (unless already changed)
            if ($tentacule->getKraken() === $this) {
                $tentacule->setKraken(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Pouvoir[]
     */
    public function getPouvoirs(): Collection
    {
        return $this->Pouvoirs;
    }

    public function addPouvoir(Pouvoir $pouvoir): self
    {
        if (!$this->Pouvoirs->contains($pouvoir)) {
            $this->Pouvoirs[] = $pouvoir;
            $pouvoir->setKraken($this);
        }

        return $this;
    }

    public function removePouvoir(Pouvoir $pouvoir): self
    {
        if ($this->Pouvoirs->removeElement($pouvoir)) {
            // set the owning side to null (unless already changed)
            if ($pouvoir->getKraken() === $this) {
                $pouvoir->setKraken(null);
            }
        }

        return $this;
    }
}
