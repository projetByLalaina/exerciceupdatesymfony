<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210316105741 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE kraken (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, age NUMERIC(10, 0) NOT NULL, taille NUMERIC(10, 0) NOT NULL, poids NUMERIC(10, 0) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pouvoir (id INT AUTO_INCREMENT NOT NULL, kraken_id INT NOT NULL, nom VARCHAR(255) NOT NULL, INDEX IDX_BE7F6EC68A9341DD (kraken_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tentacule (id INT AUTO_INCREMENT NOT NULL, kraken_id INT DEFAULT NULL, nom VARCHAR(255) NOT NULL, INDEX IDX_B31C4D108A9341DD (kraken_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE pouvoir ADD CONSTRAINT FK_BE7F6EC68A9341DD FOREIGN KEY (kraken_id) REFERENCES kraken (id)');
        $this->addSql('ALTER TABLE tentacule ADD CONSTRAINT FK_B31C4D108A9341DD FOREIGN KEY (kraken_id) REFERENCES kraken (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pouvoir DROP FOREIGN KEY FK_BE7F6EC68A9341DD');
        $this->addSql('ALTER TABLE tentacule DROP FOREIGN KEY FK_B31C4D108A9341DD');
        $this->addSql('DROP TABLE kraken');
        $this->addSql('DROP TABLE pouvoir');
        $this->addSql('DROP TABLE tentacule');
    }
}
